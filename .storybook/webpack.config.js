const pathRegex = new RegExp(`\/src\/components\\/[^/]+\\/([^/]+\\/)*stories.js$`);

module.exports = function({ config }) {
    config.module.rules.push({
        test: pathRegex,
        loaders: [
            {
                loader: require.resolve('@storybook/addon-storysource/loader'),
                options: {
                    prettierConfig: {
                        printWidth: 80,
                        singleQuote: false,
                    },
                },
            },
        ],
        enforce: 'pre',
    });

    return config;
};