# Button

To be used such as:

```jsx
<Button onClick={handleClick}>Save</Button>
```

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores eligendi facilis incidunt ipsum iure laboriosam, maiores nostrum qui soluta temporibus. Adipisci dolor facilis fugiat illum nemo quasi recusandae similique suscipit?