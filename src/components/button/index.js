import React from 'react';
import styled from 'styled-components';
import { COLOR, FONT_SIZE, BREAKPOINT } from "../../constants";

const StyledButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.bgColor ? props.bgColor : COLOR.WHITE};
  font-size: ${props => props.size ? props.size + 'px' : FONT_SIZE.MEDIUM};
  padding: 5px 12px;
  margin: 0;
  border: 1px solid ${COLOR.PRIMARY};
  font-weight: ${props => props.bold ? 'bold' : undefined };
  border-radius: 1px;
  cursor: pointer;
  width: 100%;

  @media (min-width: ${BREAKPOINT.TABLET}) {
    max-width: 420px;
    margin: 0 auto;
  }

  :hover:not(:disabled) {
    border-color: ${COLOR.RED};
  }

  :disabled {
    opacity: .15;
    cursor: not-allowed ;
  }
`;

const Button = ({
    children,
    onClick,
    disabled,
    bgColor,
    primary,
    size,
    bold,
}) => (
    <StyledButton
        primary={primary}
        onClick={onClick}
        bgColor={bgColor}
        size={size}
        bold={bold}
        disabled={disabled}>
        {children}
    </StyledButton>
);

export default Button;
