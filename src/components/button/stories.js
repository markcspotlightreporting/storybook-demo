import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { INITIAL_VIEWPORTS } from "@storybook/addon-viewport/src/defaults";

import Button from '.';
import notes from './README.md';

const stories = storiesOf('Button', module);

stories.addDecorator(withKnobs);
stories.addParameters({
    backgrounds: [
        { name: 'dark', value: '#263238' },
        { name: 'light', value: '#F6F6F6', default: true },
    ],
    viewport: INITIAL_VIEWPORTS,
});

stories
    .add('button (default)', () =>
        <Button
            onClick={action('clicked: click me button')}
            bgColor={text('color', '#fff')}
            size={text('font size', '14')}
            bold={boolean('bold', false)}
            disabled={boolean('Disabled', false)}>
            click me 😀 😎 👍 💯
        </Button>,
        {
            notes
        }
    )
    .add('button disabled', () =>
        <Button
            disabled={boolean('Disabled', true)}>
           submit 😀 🤩 ✊ ❗️
        </Button>
);
