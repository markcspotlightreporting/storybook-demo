import React from 'react';
import { mount } from 'enzyme';

import Button from '.';

describe('Button', () => {
    describe('basics', () => {
        it('should render a button', () => {
            const wrapper = mount(<Button />);

            expect(wrapper.find('button')).toBeTruthy();
        });

        it('matches snapshot', () => {
            const wrapper = mount(<Button />);

            expect(wrapper).toMatchSnapshot();
        });
    });

    describe('disabled', () => {
        it('should render a button with the disabled attribute', () => {
            const wrapper = mount(<Button disabled />);

            expect(wrapper.find('button').prop('disabled')).toBeTruthy();
        });

        it('matches snapshot', () => {
            const wrapper = mount(<Button disabled />);

            expect(wrapper).toMatchSnapshot();
        });
    });
});
