import React from 'react';
import styled from 'styled-components';
import { COLOR, FONT_SIZE, BREAKPOINT } from "../../constants";

const StyledButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #efefef;
  font-size: ${props => props.size ? props.size + 'px' : FONT_SIZE.XX_LARGE};
  padding: 130px 0;
  margin: 0;
  border: 1px solid ${COLOR.PRIMARY};
  font-weight: ${props => props.bold ? 'bold' : undefined };
  border-radius:50%;
  cursor: pointer;
  height: 100%;
  width: 100%;

  @media (min-width: ${BREAKPOINT.TABLET}) {
    max-width: 420px;
    padding: 170px 0;
    margin: 0 auto;
  }

  :hover:not(:disabled) {
    border-color: ${COLOR.RED};
  }

  :disabled {
    opacity: .15;
    cursor: not-allowed ;
  }
`;

const Button = ({
    children,
    onClick,
    disabled,
    bgColor,
    primary,
    size,
    bold,
}) => (
    <StyledButton
        primary={primary}
        onClick={onClick}
        bgColor={bgColor}
        size={size}
        bold={bold}
        disabled={disabled}>
        {children}
    </StyledButton>
);

export default Button;
