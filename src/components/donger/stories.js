import React from 'react';
import { storiesOf } from '@storybook/react';

import Donger from '.';
import notes from './README.md';

const stories = storiesOf('Donger', module);

stories
    .add('hi there', () =>
        <Donger>
            ᕙ(˵ ಠ ਊ ಠ ˵)ᕗ
        </Donger>,
        {
            notes
        }
    )
    .add('hello', () =>
        <Donger>
            ┌། ☯ _ʖ ☯ །┐️
        </Donger>,
        {
            notes
        }
);
