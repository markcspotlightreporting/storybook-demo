import React from 'react';
import { mount } from 'enzyme';

import Donger from '.';

describe('Donger', () => {
    it('should render a donger', () => {
        const wrapper = mount(<Donger>Test</Donger>);

        expect(wrapper.find('div')).toBeTruthy();
    });

    it('matches snapshot', () => {
        const wrapper = mount(<Donger>Example</Donger>);

        expect(wrapper).toMatchSnapshot();
    });
});
