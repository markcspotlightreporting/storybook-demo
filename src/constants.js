export const COLOR = {
    RED: '#ea4e61',
    GREEN: '#17c66a',
    BLUE: '#1bb5ea',
    BLACK: '#111',
    WHITE: '#cfcfcf',
    PRIMARY: '#1a2952',
    SECONDARY: '#1cb5ea',
};

export const FONT_SIZE = {
    X_SMALL: '8px',
    SMALL: '12px',
    MEDIUM: '16px',
    LARGE: '18px',
    X_LARGE: '22px',
    XX_LARGE: '36px',
};

export const BREAKPOINT = {
    MOBILE: '320px',
    TABLET: '641px',
    DESKTOP: '769px',
};
